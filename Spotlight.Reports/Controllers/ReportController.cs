﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Spotlight.Reports.Filters;
using Spotlight.Reports.Models;
using SpotlightCoin.BAL;
using SpotlightCoin.Model.Domain;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace Spotlight.Reports.Controllers
{
    [SessionFilter]
    [HandleException]
    public class ReportController : Controller
    {
        private IReport _IReport;

        public ReportController(IReport report)
        {


            this._IReport = report;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        /// <summary>
        /// Need to validate the credential
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Login(UserLogin user)
        {
            UserLogin userLogin = new UserLogin();
            if (ModelState.IsValid)
            {
                //UserLogin userLogin = new UserLogin();
                userLogin.UserName = user.UserName;
                userLogin.Password = user.Password;
            }
            StringBuilder traceInformation = new StringBuilder();

            Uri basUrl = new Uri("http://localhost:52223/api/Report/Login");
            //string UserIdUrl = Common.Common.GetAPIBaseURL() + Constant.UserIdPath;

            try
            {

                if (userLogin.UserName != null & user.Password != null)
                {
                    var byteContent = JsonConvert.SerializeObject(userLogin);
                    var buffer = Encoding.UTF8.GetBytes(byteContent);

                    using (HttpClient client = new HttpClient())
                    {
                        var content = new ByteArrayContent(buffer);
                        var response = client.PostAsync(basUrl, content).Result;
                        var responseString = response.Content.ReadAsStringAsync().Result;
                        //var tokenDetails = JsonConvert.DeserializeObject<TokenData>(responseString);


                    }
                }
                else
                {
                    return View();
                }
            }
            catch
            {
                throw;
            }
            return View();
        }

        //public IActionResult Index()
        //{
        //    IEnumerable<DailyPOSRevenueFiscalYOY> data = _IReport.GetDailyPOSRevenueFiscalYOY();
        //    return View(data);
        //}

        public IActionResult GetReportList()
        {
            List<ReportList> reportList = new List<ReportList>
            {
                new ReportList() {  ReportName = "Daily POS Revenue Fiscal YOY",  ReportActionName = "GetDailyPOSRevenueFiscalYOY" },
                new ReportList() {  ReportName = "Daily Revenue Report",  ReportActionName = "GetDailyRevenueReport" },
                new ReportList() {  ReportName = "Hosted Player Activity",  ReportActionName = "GetHostedPlayerActivity" },
                new ReportList() {  ReportName = "Month End Slot Marketing Analysis",  ReportActionName = "GetMonthEndSlotMarketingAnalysis" },
                new ReportList() {  ReportName = "New Acquisition by Host by Month",  ReportActionName = "GetNewAcquisitionbyHostbyMonth" },
                new ReportList() {  ReportName = "New Member by ADT or ADA Report",  ReportActionName = "GetNewMemberbyADTorADAReport" },
                new ReportList() {  ReportName = "Patron Carded Revenue Trends",  ReportActionName = "GetPatronCardedRevenueTrends" },
                new ReportList() {  ReportName = "Patron Listing",  ReportActionName = "GetPatronListing" },
                new ReportList() {  ReportName = "Tables with Play by Day",  ReportActionName = "GetTableswithPlaybyDay" },
                new ReportList() {  ReportName = "Top Twenty Winners and Losers",  ReportActionName = "GetTopTwentyWinnersandLosers" }
            };
            return View(reportList);
        }

        public IActionResult GetDailyPOSRevenueFiscalYOY()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GetDailyPOSRevenueFiscalYOYSearch(ReportSearchEntity model)
        {
            IEnumerable<DailyPOSRevenueFiscalYOY> data = _IReport.GetDailyPOSRevenueFiscalYOY(model);
            ViewBag.data = data;
            ViewBag.searchParameter = model;
            return PartialView("_GetDailyPOSRevenueFiscalYOYPartial");
        }
        public IActionResult ExportToExcel()
        {
            return View("_GetDailyPOSRevenueFiscalYOYPartial");
            //return View(SampleData.DataGridEmployees.Take(10).ToList<Employee>());
        }


        public IActionResult GetDailyRevenueReport()
        {
            return View();
        }

        public IActionResult GetDailyRevenueReportSearch(ReportSearchEntity model)
        {
            IEnumerable<DailyRevenueReport> data = _IReport.GetDailyRevenueReport(model);
            ViewBag.data = data;
            ViewBag.searchParameter = model;
            return PartialView("_GetDailyRevenueReportPartial");
        }

        public IActionResult GetHostedPlayerActivity()
        {

            return View();
        }

        public IActionResult GetHostedPlayerActivitySearch(ReportSearchEntity model)
        {
            IEnumerable<HostedPlayerActivity> data = _IReport.GetHostedPlayerActivity(model);
            ViewBag.data = data;
            ViewBag.searchParameter = model;
            return PartialView("_GetHostedPlayerActivityPartial");
        }

        public IActionResult GetMonthEndSlotMarketingAnalysis()
        {
            ChartInpuControlData data = _IReport.GetChartInputData();
            ViewBag.data = data;
            return View();
        }


        public IActionResult GetMonthEndSlotMarketingAnalysisSearch(ReportSearchEntity model)
        {

            ChartData data = _IReport.GetChartData(model);
            ViewBag.data = data;
            ViewBag.searchParameter = model;
            return PartialView("_GetMonthEndSlotMarketingAnalysisPartial");
        }

        public IActionResult GetNewAcquisitionbyHostbyMonth()
        {
            return View();
        }

        public IActionResult GetNewAcquisitionbyHostbyMonthSearch(ReportSearchEntity model)
        {
            IEnumerable<NewAcquisitionbyHostbyMonth> data = _IReport.GetNewAcquisitionbyHostbyMonth(model);
            ViewBag.data = data;
            ViewBag.searchParameter = model;
            return PartialView("_GetNewAcquisitionbyHostbyMonthPartial");
        }

        public IActionResult GetNewMemberbyADTorADAReport()
        {
            //IEnumerable<NewMemberbyADTorADAReport> data = _IReport.GetNewMemberbyADTorADAReport();
            return View();
        }

        public IActionResult GetNewMemberbyADTorADAReportSearch(ReportSearchEntity model)
        {
            IEnumerable<NewMemberbyADTorADAReport> data = _IReport.GetNewMemberbyADTorADAReport(model);
            ViewBag.data = data;
            ViewBag.ReportType = model.Type;
            ViewBag.searchParameter = model;
            return PartialView("_GetNewMemberbyADTorADAReportPartial");
        }

        public IActionResult GetPatronCardedRevenueTrends()
        {
            //IEnumerable<PatronCardedRevenueTrends> data = _IReport.GetPatronCardedRevenueTrends();
            return View();
        }

        public IActionResult GetPatronCardedRevenueTrendsSearch(ReportSearchEntity model)
        {
            IEnumerable<PatronCardedRevenueTrends> data = _IReport.GetPatronCardedRevenueTrends(model);
            ViewBag.data = data;
            ViewBag.ReportType = model.ReportType;
            ViewBag.searchParameter = model;
            return PartialView("_GetPatronCardedRevenueTrendsPartial");
        }

        public IActionResult GetPatronListing()
        {
            //IEnumerable<PatronListingMain> data = _IReport.GetPatronListing();
            return View();
        }

        public IActionResult GetPatronListingSearch(ReportSearchEntity model)
        {
            IEnumerable<PatronListingMain> data = _IReport.GetPatronListing(model);
            ViewBag.data = data;
            ViewBag.searchParameter = model;
            return PartialView("_GetPatronListingPartial");
        }


        public IActionResult GetTableswithPlaybyDay()
        {
            //IEnumerable<TableswithPlaybyDay> data = _IReport.GetTableswithPlaybyDay();
            return View();
        }

        public IActionResult GetTableswithPlaybyDaySearch(ReportSearchEntity model)
        {
            IEnumerable<TableswithPlaybyDay> data = _IReport.GetTableswithPlaybyDay(model);
            ViewBag.data = data;
            ViewBag.searchParameter = model;
            return PartialView("_GetTableswithPlaybyDayPartial");
        }


        public IActionResult GetTopTwentyWinnersandLosers()
        {
            //IEnumerable<TopTwentyWinnersandLosers> data = _IReport.GetTopTwentyWinnersandLosers();
            return View();
        }

        public IActionResult GetTopTwentyWinnersandLosersSearch(ReportSearchEntity model)
        {
            IEnumerable<TopTwentyWinnersandLosers> data = _IReport.GetTopTwentyWinnersandLosers(model);
            ViewBag.data = data;
            ViewBag.ReportType = model.ReportType;
            ViewBag.searchParameter = model;
            return PartialView("_GetTopTwentyWinnersandLosersPartial");
        }

        public IActionResult GetHostListMultiple(ReportSearchEntity model)
        {
            IEnumerable<HostedPlayerActivityHost> data = _IReport.GetHostList(model);
            ViewBag.searchParameter = model;
            return PartialView("_HostListMultiplePartial", data);
        }

        public IActionResult GetHostList(ReportSearchEntity model)
        {
            IEnumerable<HostedPlayerActivityHost> data = _IReport.GetHostList(model);

            ViewBag.searchParameter = model;
            return PartialView("_HostListPartial", data);
        }
    }


}