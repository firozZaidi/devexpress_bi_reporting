﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SpotlightCoin.BAL;
using SpotlightCoin.Model.Domain;
using System.Collections.Generic;

namespace Spotlight.Reports.Controllers
{
    public class AccountController : Controller
    {
        private readonly IHostManager _IHostManager;
        List<SelectListItem> hostList = new List<SelectListItem>();
        public AccountController(IHostManager iHostManager)
        {
            this._IHostManager = iHostManager;
        }
        public IActionResult Login()
        {
            GetHost();
            Host model = new Host();
            return View(model);
        }


        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            GetHost();
            Host model = new Host();
            return View("~/Views/Account/Login.cshtml", model);
        }


        [HttpPost]
        public IActionResult Login(Host host)
        {
            if (string.IsNullOrEmpty(host.LDAPUserName))
            {
                ViewBag.Message = "Host does not have LDAP User name.";
                GetHost();
                return View(host);
            }
            if (_IHostManager.IsValidLogin(host))
            {
                HttpContext.Session.SetString("User", host.LDAPUserName);
                return RedirectToAction("GetReportList", "Report");
            }
            ViewBag.Message = "Provided passsword is invalid.";
            GetHost();
            return View(host);
        }

        private void GetHost()
        {
            IEnumerable<Host> Hosts = _IHostManager.GetAllHosts();
            List<SelectListItem> hostList = new List<SelectListItem>();

            foreach (Host host in Hosts)
            {
                hostList.Add(new SelectListItem { Text = host.HostFirstName + " " + host.HostLastName, Value = host.LDAPUserName });
            }
            ViewBag.HostList = hostList;
        }
    }
}