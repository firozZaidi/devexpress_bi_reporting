

$(document).ready(function () {
    $(".menu-toggle").click(function (e) {
        e.preventDefault();
        $("body").toggleClass("toggled");
        $(".menu-toggle .zmdi").toggleClass("zmdi-arrow-right");
    });

    if (menuType === 'ReportList') {
        $('#sideMenu1').addClass('active');
        $('#sideMenu2').removeClass('active');
    }

});