﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Spotlight.Reports.Filters
{
    public class SessionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {

            if (string.IsNullOrEmpty(context.HttpContext.Session.GetString("User")))
            {
                context.Result = new RedirectToRouteResult(
                           new RouteValueDictionary
                           {
                                    { "controller", "Account" },
                                    { "action", "Login" }
                           });
            }
        }
    }
}
