﻿namespace Spotlight.Reports.Models
{
    public class ReportList
    {
        public string ReportName { get; set; }
        public string ReportActionName { get; set; }
    }
}
