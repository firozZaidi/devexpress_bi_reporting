﻿using System.ComponentModel.DataAnnotations;

namespace Spotlight.Reports.Models
{
    public class UserLogin
    {
        [Required]
        [MaxLength(20)]
        [Display(Name ="User Name")]
        public string UserName { get; set; }

        [Required]
        [MaxLength(20)]
        [DataType(DataType.Password)]
        public string  Password { get; set; }
    }
}
