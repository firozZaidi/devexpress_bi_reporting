﻿namespace SpotlightCoin.Model.Domain
{
    public class PatronListingHost
    {
        public int HostID { get; set; }
        public string HostName { get; set; }
    }
}
