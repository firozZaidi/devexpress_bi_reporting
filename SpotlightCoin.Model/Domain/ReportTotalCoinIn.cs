﻿using System;

namespace SpotlightCoin.Model.Domain
{
    public partial class ReportTotalCoinIn
    {
        public string Month { get; set; }
        public int Year { get; set; }
        public int AnalysisMonth { get; set; }
        public Nullable<int> YearCode { get; set; }
        public string MonthShortName { get; set; }
        public decimal TheoNetFP { get; set; }
        public decimal CoinIn { get; set; }
    }
}
