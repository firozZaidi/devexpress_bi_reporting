﻿using System;

namespace SpotlightCoin.Model.Domain
{
    public class ReportYearStart
    {
        public int Year { get; set; }
        public Nullable<int> YearCode { get; set; }
    }
}
