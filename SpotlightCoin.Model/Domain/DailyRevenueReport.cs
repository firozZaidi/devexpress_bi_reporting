﻿using System;

namespace SpotlightCoin.Model.Domain
{
    public class DailyRevenueReport
    {

        public DateTime Dates { get; set; }
        public string DateToDiplay { get { return string.Format("{0:MM/dd/yyyy}", Dates); } }
        public int PropertyId { get; set; }
        public int OutletID { get; set; }
        public string OutletName { get; set; }
        public int AllCovers { get; set; }
        public float TotalRevvenue { get; set; }
        public int Checks { get; set; }
    }
}
