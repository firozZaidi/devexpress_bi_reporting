﻿using System;

namespace SpotlightCoin.Model.Domain
{
    public class ReportSearchEntity
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int PropertyId { get; set; }
        public string HostId { get; set; }

        public int Amount { get; set; }
        public int Type { get; set; }
        public int ReportType { get; set; }
        public int Birth { get; set; }
        public int Anniversary { get; set; }
        public int Visits { get; set; }
        public int Points { get; set; }
        public int Theo { get; set; }
        public int ADT { get; set; }
        public int Host { get; set; }
        public int Begin { get; set; }
        public int End { get; set; }
    }
}
