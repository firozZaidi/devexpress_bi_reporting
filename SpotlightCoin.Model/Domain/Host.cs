﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SpotlightCoin.Model.Domain
{
    public class Host
    {
        public string ActiveFlag { get; set; }
        public string HostEmail { get; set; }
        public string HostFirstName { get; set; }
        public string HostLastName { get; set; }
        [Display(Name = "Password")]
        [Required(ErrorMessage = "Please provide password")]
        public string HostPassword { get; set; }
        [Required(ErrorMessage = "Please select user")]
        public string LDAPUserName { get; set; }
        public int HostUserId { get; set; }
        [Display(Name ="User")]
       
        public string HostUserName { get; set; }
        public int HostsId { get; set; }
    }
}
