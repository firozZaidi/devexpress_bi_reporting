﻿namespace SpotlightCoin.Model.Domain
{
    public class HostedPlayerActivityHost
    {
        public int HostID { get; set; }
        public string HostName { get; set; }
    }
}
