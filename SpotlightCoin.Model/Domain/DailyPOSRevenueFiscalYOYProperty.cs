﻿using System.Collections.Generic;

namespace SpotlightCoin.Model.Domain
{
    public class DailyPOSRevenueFiscalYOYProperty
    {
        public List<DailyPOSRevenueFiscalYOY> DailyPOSRevenueFiscalYOY { get;  }
        public int PropertyID { get; set; }
        public string PropertyName { get; set; }
        public string Domain { get; set; }
        public string ReportVersion { get; set; }
    }
}
