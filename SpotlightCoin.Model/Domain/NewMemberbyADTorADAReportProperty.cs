﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotlightCoin.Model.Domain
{
    public class NewMemberbyADTorADAReportProperty
    {
        public List<NewMemberbyADTorADAReport> NewMemberbyADTorADAReport { get; }
        public int PropertyID { get; set; }
        public string PropertyName { get; set; }
        public string Domain { get; set; }
        public string ReportVersion { get; set; }
    }
}
