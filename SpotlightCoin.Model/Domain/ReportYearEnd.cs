﻿using System;

namespace SpotlightCoin.Model.Domain
{
    public class ReportYearEnd
    {
        public int Year { get; set; }
        public Nullable<int> YearCode { get; set; }
    }
}
