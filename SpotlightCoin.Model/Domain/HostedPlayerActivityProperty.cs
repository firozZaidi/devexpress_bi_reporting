﻿using System.Collections.Generic;

namespace SpotlightCoin.Model.Domain
{
    public class HostedPlayerActivityProperty
    {
        public List<HostedPlayerActivity> HostedPlayerActivity { get; }
        public List<HostedPlayerActivityHost> HostedPlayerActivityHost { get; }
        public int PropertyID { get; set; }
        public string PropertyName { get; set; }
        public string Domain { get; set; }
        public string ReportVersion { get; set; }
    }
}
