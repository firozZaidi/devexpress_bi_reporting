﻿namespace SpotlightCoin.Model.Domain
{
    public class TopTwentyWinnersandLosers
    {
        public int PlayerID { get; set; }
        public string FName { get; set; }
        public float SlotWin { get; set; }
        public string LName { get; set; }
        public float TableWin { get; set; }
        public float TotalWin { get; set; }
        public int PlayerID1 { get; set; }
        public string FName1 { get; set; }
        public float SlotWin1 { get; set; }
        public string LName1 { get; set; }
        public float TableWin1 { get; set; }
        public float TotalWin1 { get; set; }
        public string Blank { get; set; }
    }
}
