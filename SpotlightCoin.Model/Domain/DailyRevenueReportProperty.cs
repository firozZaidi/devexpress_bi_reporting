﻿using System.Collections.Generic;

namespace SpotlightCoin.Model.Domain
{
    public class DailyRevenueReportProperty
    {
        private List<DailyRevenueReport> _DailyRevenueReport;
        public List<DailyRevenueReport> DailyRevenueReport { get { return _DailyRevenueReport; } }
        public int PropertyID { get; set; }
        public string PropertyName { get; set; }
        public string Domain { get; set; }
        public string ReportVersion { get; set; }

        public void AddDailyRevenueReport(List<DailyRevenueReport> dailyRevenueReport)
        {
            _DailyRevenueReport = dailyRevenueReport;
        }
    }
}
