﻿namespace SpotlightCoin.Model.Domain
{
    public class PatronCardedRevenueTrends
    {
        
        public string MMMYYYY { get; set; }
        public float CYTheo { get; set; }
        public float CYWin { get; set; }
        public string LYMMMYYYY { get; set; }
        public float LYTheo { get; set; }
        public float LYWin { get; set; }
        public string GrowthTheo { get; set; }
        public string GrowthWin { get; set; }
        public float ADT { get; set; }
        public float ADA { get; set; }
        public float PriorADT { get; set; }
        public float PriorADA { get; set; }
        public string GrowthADT { get; set; }
        public string GrowthADA { get; set; }
        public string Blank { get; set; }
    }
}
