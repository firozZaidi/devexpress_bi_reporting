﻿using System;

namespace SpotlightCoin.Model.Domain
{
    public class ReportFiscalNewSignups
    {
        public string FiscalMonth { get; set; }
        public Nullable<int> FiscalMonthNumOfYear { get; set; }
        public Nullable<int> FiscalYear { get; set; }
        public Nullable<int> YearCode { get; set; }
        public int NewSignups { get; set; }
        public Nullable<decimal> YOYPercentSignups { get; set; }

        public string MonthName { get; set; }
    }
}
