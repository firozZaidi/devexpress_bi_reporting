﻿namespace SpotlightCoin.Model.Domain
{
    public class NewAcquisitionbyHostbyMonth
    {
        public int PlayerID { get; set; }
        public int PropertyId { get; set; }
        public string AnalysisYear { get; set; }
        public string AnalysisMonth { get; set; }
        public string PlayerName { get; set; }
        public int HostUserId { get; set; }
        public string CurrentHostName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string BHActualWin { get; set; }
        public string BHTheoWin { get; set; }
        public int BHVisits { get; set; }
        public int AHVisits { get; set; }
        public int AHActualWin { get; set; }
        public float AHTheoWin { get; set; }
        public string BHADT { get; set; }
        public string AHADT { get; set; }
        public int LTDActualWin { get; set; }
        public string LTDTheoWin { get; set; }
        public string LTDVisits { get; set; }
        public int LTDADT { get; set; }
        public string LastVisit { get; set; }
        public string ActualDiff { get; set; }
        public string TheoDiff { get; set; }
        public string VisitsDiff { get; set; }
        public string ADTDiff { get; set; }

    }
}
