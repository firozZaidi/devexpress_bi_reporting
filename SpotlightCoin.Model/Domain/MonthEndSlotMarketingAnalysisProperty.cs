﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotlightCoin.Model.Domain
{
    public class MonthEndSlotMarketingAnalysisProperty
    {
        public List<MonthEndSlotMarketingAnalysisMain> MonthEndSlotMarketingAnalysisMain { get;  }
        public List<MonthEndSlotMarketingAnalysisTotalCoinIn> MonthEndSlotMarketingAnalysisTotalCoinIn { get;  }
        public List<MonthEndSlotMarketingAnalysisFiscalNewSignups> MonthEndSlotMarketingAnalysisFiscalNewSignups { get;  }
        public List<MonthEndSlotMarketingAnalysisYears> MonthEndSlotMarketingAnalysisYears { get; }
        public List<MonthEndSlotMarketingAnalysisYears2> MonthEndSlotMarketingAnalysisYears2 { get; }
        public int PropertyID { get; set; }
        public string PropertyName { get; set; }
        public string Domain { get; set; }
        public string ReportVersion { get; set; }
    }
}
