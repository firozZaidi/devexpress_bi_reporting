﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotlightCoin.Model.Domain
{
    public class MonthEndSlotMarketingAnalysisMain
    {
        public string Month { get; set; }
        public string Year { get; set; }
        public int YearCode { get; set; }
        public string AnalysisMonth { get; set; }
        public string MonthShortName { get; set; }
        public int RatedPatronCount { get; set; }
        public string GamingVisits { get; set; }
        public float TheoWin { get; set; }
        public float FreePlayDollarsDownloaded { get; set; }
        public float TheoNetFP { get; set; }
        public float CashIn { get; set; }
        public float AvgDays { get; set; }
        public float AverageDailyTheoWin { get; set; }
        public float FPPctofTheo { get; set; }
        public int NewSignups { get; set; }
    }
}
