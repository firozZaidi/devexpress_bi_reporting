﻿namespace SpotlightCoin.Model.Domain
{
    public class PatronListingMain
    {
        public int PlayerID { get; set; }
        public int AccountID { get; set; }
        public string LastName { get; set; }
        public string HostName { get; set; }
        public string FirstName { get; set; }
        public string ClubLevel { get; set; }
        public string City { get; set; }
        public string Rank { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }
        public string MailFlag { get; set; }
        public string Anniversary { get; set; }
        public string Phone { get; set; }
        public string State { get; set; }
        public string SystemRank { get; set; }
        public string Referral { get; set; }
        public string ManualRank { get; set; }
        public string Email { get; set; }
        public string Birthday { get; set; }
        public int BirthMonth { get; set; }
        public int PointsEarned { get; set; }
        public int Trips { get; set; }
        public float Theo { get; set; }
        public float ADT { get; set; }
        public string LastPlayDate { get; set; }
    }
}
