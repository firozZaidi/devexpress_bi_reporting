﻿using System;

namespace SpotlightCoin.Model.Domain
{
    public  class ReportMain
    {
        public string Month { get; set; }
        public int Year { get; set; }
        public int AnalysisMonth { get; set; }
        public Nullable<int> YearCode { get; set; }
        public string MonthShortName { get; set; }
        public int RatedPatronCount { get; set; }
        public int GamingVisits { get; set; }
        public decimal TheoWin { get; set; }
        public decimal FreePlayDollarsDownloaded { get; set; }
        public decimal TheoNetFP { get; set; }
        public decimal CashIn { get; set; }
        public double AvgDays { get; set; }
        public decimal AverageDailyTheoWin { get; set; }
        public double FPPctofTheo { get; set; }
        public int NewSignups { get; set; }
    }
}
