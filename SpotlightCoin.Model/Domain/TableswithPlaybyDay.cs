﻿using System;

namespace SpotlightCoin.Model.Domain
{
    public class TableswithPlaybyDay
    {
        private string _date;
        public string AuditDate
        {
            get
            {
                DateTime dt = Convert.ToDateTime(_date).Date;
                return dt.ToShortDateString();
            }
            set
            {
                _date = value;
            }
        }
        public int TablesWithPlay { get; set; }
    }
}
