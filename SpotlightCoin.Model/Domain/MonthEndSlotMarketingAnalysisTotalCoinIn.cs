﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotlightCoin.Model.Domain
{
    public class MonthEndSlotMarketingAnalysisTotalCoinIn
    {
        public string Month { get; set; }
        public string Year { get; set; }
        public int YearCode { get; set; }
        public int AnalysisMonth { get; set; }
        public string MonthShortName { get; set; }
        public float TheoNetFP { get; set; }
        public float CoinIn { get; set; }
    }
}
