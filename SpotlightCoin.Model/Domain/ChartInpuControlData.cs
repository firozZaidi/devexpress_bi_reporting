﻿using System.Collections.Generic;

namespace SpotlightCoin.Model.Domain
{
    public class ChartInpuControlData
    {

        public IEnumerable<ReportYearEnd> ReportYearEnd { get; private set; }
        public IEnumerable<ReportYearStart> ReportYearStart { get; private set; }
        public void AddData(IEnumerable<ReportYearStart> reportYearStart, IEnumerable<ReportYearEnd> reportYearEnd)
        {
            this.ReportYearStart = reportYearStart;
            this.ReportYearEnd = reportYearEnd;
        }
    }
}
