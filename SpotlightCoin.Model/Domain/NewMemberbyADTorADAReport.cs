﻿namespace SpotlightCoin.Model.Domain
{
    public class NewMemberbyADTorADAReport
    {
        public int PlayerID { get; set; }
        public int PropertyId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public float PointsBalace { get; set; }
        public string EMail { get; set; }
        public string AccountStatusId { get; set; }
        public string CardLevel { get; set; }
        public string MailFlag { get; set; }
        public string Address1A { get; set; }
        public string Address1B { get; set; }
        public string City1 { get; set; }
        public string State1 { get; set; }
        public string Zip1 { get; set; }
        public string HomePhone1 { get; set; }
        public float CompBalance { get; set; }
        public string LastVisit { get; set; }
        public float ADT { get; set; }
        public string EntryDate { get; set; }
        public float ActualWin { get; set; }
        public float Theo { get; set; }
        public int Trips { get; set; }
        public float ADA { get; set; }
        public string PlayerType { get; set; }
        public string EntryDiff { get; set; }
    }
}
