﻿namespace SpotlightCoin.Model.Domain
{
    public class HostedPlayerActivity
    {
        public int PlayerID { get; set; }
        public int AccountID { get; set; }
        public string FullName { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public int HostUserId { get; set; }
        public string HostName { get; set; }
        public string HostStartDate { get; set; }
        public string HostEndDate { get; set; }
        public string Referral { get; set; }
        public string ClubLevel { get; set; }
        public int LYDays { get; set; }
        public int CYDays { get; set; }
        public int DaysDiff { get; set; }
        public float LYCI { get; set; }
        public float CYCI { get; set; }
        public float CIDiff { get; set; }
        public float LYCO { get; set; }
        public float CYCO { get; set; }
        public float CODiff { get; set; }
        public float LYJP { get; set; }
        public float CYJP { get; set; }
        public float JPDiff { get; set; }
        public float LYTheo { get; set; }
        public float LYPBT { get; set; }
        public float LYTheoNet { get; set; }
        public float CYTheo { get; set; }
        public float CYPBT { get; set; }
        public float CYTheoNet { get; set; }
        public float LYActual { get; set; }
        public float LYActualNet { get; set; }
        public float CYActual { get; set; }
        public float CYActualNet { get; set; }
        public float LYADT { get; set; }
        public float CYADT { get; set; }
        public float ADTDiff { get; set; }
        public string LYLPD { get; set; }
        public string CYLPD { get; set; }
        public float TheoDiff { get; set; }
    }
}
