﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotlightCoin.Model.Domain
{
    public class MonthEndSlotMarketingAnalysisYears2
    {
        public int Year { get; set; }
        public int YearCode { get; set; }
    }
}
