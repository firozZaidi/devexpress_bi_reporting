﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotlightCoin.Model.Domain
{
    public class MonthEndSlotMarketingAnalysisYears
    {
        public int Year { get; set; }
        public int YearCode { get; set; }
    }
}
