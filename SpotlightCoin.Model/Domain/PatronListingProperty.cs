﻿using System.Collections.Generic;

namespace SpotlightCoin.Model.Domain
{
    public class PatronListingProperty
    {
        public List<PatronListingMain> PatronListingMain { get;  }
        public List<PatronListingHost> PatronListingHost { get; }
        public int PropertyID { get; set; }
        public string PropertyName { get; set; }
        public string Domain { get; set; }
        public string ReportVersion { get; set; }
    }
}
