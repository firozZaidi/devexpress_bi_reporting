﻿using System.Collections.Generic;

namespace SpotlightCoin.Model.Domain
{
    public class PatronCardedRevenueTrendsProperty
    {
        public List<PatronCardedRevenueTrends> PatronCardedRevenueTrends { get;  }
        public int PropertyID { get; set; }
        public string PropertyName { get; set; }
        public string Domain { get; set; }
        public string ReportVersion { get; set; }
    }
}
