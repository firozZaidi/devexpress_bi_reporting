﻿namespace SpotlightCoin.Model.Domain
{
    public class DailyPOSRevenueFiscalYOY
    {
        public string Date { get; set; }
        public string FiscalYear { get; set; }
        public int PropertyId { get; set; }
        public int OutletID { get; set; }
        public string OutletName { get; set; }
        public int AllCovers { get; set; }
        public float TotalRevvenue { get; set; }
        public int Checks { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
    }
}
