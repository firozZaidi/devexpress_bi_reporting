﻿using System.Collections.Generic;

namespace SpotlightCoin.Model.Domain
{
    public class ChartData
    {

        public IEnumerable<ReportMain> ReportMains { get; private set; }
        public IEnumerable<ReportFiscalNewSignups> ReportFiscalNewSignups { get; private set; }
        public IEnumerable<ReportTotalCoinIn> ReportTotalCoinIns { get; private set; }

        public void AddData(IEnumerable<ReportMain> reportMains, IEnumerable<ReportFiscalNewSignups> reportFiscalNewSignups, IEnumerable<ReportTotalCoinIn> reportTotalCoinIns)
        {
            this.ReportMains = reportMains;
            this.ReportFiscalNewSignups = reportFiscalNewSignups;
            this.ReportTotalCoinIns = reportTotalCoinIns;
        }
    }
}
