﻿using System;
using System.Collections.Generic;
using System.Text;
using SpotlightCoin.DAL.Repositories;
using SpotlightCoin.Model.Domain;

namespace SpotlightCoin.BAL
{
    public class HostManager : IHostManager
    {
        private readonly IHostRepository _IHostRepository;

        public HostManager(IHostRepository iHostRepository)
        {
            this._IHostRepository = iHostRepository;
        }
        public IEnumerable<Host> GetAllHosts()
        {
            return _IHostRepository.GetAllHost();
        }

        public bool IsValidLogin(Host host)
        {
            return _IHostRepository.IsValidLogin(host);
        }
    }
}
