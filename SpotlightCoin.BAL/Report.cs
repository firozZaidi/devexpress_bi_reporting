﻿using SpotlightCoin.DAL.Repositories;
using SpotlightCoin.Model.Domain;
using System;
using System.Collections.Generic;

namespace SpotlightCoin.BAL
{
    public class Report : IReport
    {
        private readonly IGenericRepository _genericRepository;
        public Report(IGenericRepository genericRepository)
        {
            _genericRepository = genericRepository;
        }

        public IEnumerable<HostedPlayerActivityHost> GetHostList(ReportSearchEntity model)
        {

            object parameters = new { @StartDate = Convert.ToDateTime(model.StartDate), @EndDate = Convert.ToDateTime(model.EndDate) };
            return _genericRepository.SpExecuteSql<HostedPlayerActivityHost>(Constant.SPHostList, parameters);
        }
        public IEnumerable<DailyRevenueReport> GetDailyRevenueReport(ReportSearchEntity model)
        {

            object parameters = new { @StartDate = Convert.ToDateTime(model.StartDate), @EndDate = Convert.ToDateTime(model.EndDate) };

            return _genericRepository.SpExecuteSql<DailyRevenueReport>(Constant.SPDailyRevenueReport, parameters);
        }
        public IEnumerable<DailyPOSRevenueFiscalYOY> GetDailyPOSRevenueFiscalYOY(ReportSearchEntity model)
        {


            object parameters = new { @StartDate = Convert.ToDateTime(model.StartDate), @EndDate = Convert.ToDateTime(model.EndDate) };


            return _genericRepository.SpExecuteSql<DailyPOSRevenueFiscalYOY>(Constant.SPDailyPOSRevenueFiscalYOY, parameters);
        }
        public IEnumerable<TableswithPlaybyDay> GetTableswithPlaybyDay(ReportSearchEntity model)
        {

            object parameters = new { @StartDate = Convert.ToDateTime(model.StartDate), @EndDate = Convert.ToDateTime(model.EndDate) };
            return _genericRepository.SpExecuteSql<TableswithPlaybyDay>(Constant.SPTableswithPlaybyDay, parameters);
        }
        public IEnumerable<NewAcquisitionbyHostbyMonth> GetNewAcquisitionbyHostbyMonth(ReportSearchEntity model)
        {


            object parameters = new { @StartDate = Convert.ToDateTime(model.StartDate), @EndDate = Convert.ToDateTime(model.EndDate) };
            return _genericRepository.SpExecuteSql<NewAcquisitionbyHostbyMonth>(Constant.SPNewAcquisitionbyHostbyMonth, parameters);
        }
       
        public IEnumerable<TopTwentyWinnersandLosers> GetTopTwentyWinnersandLosers(ReportSearchEntity model)
        {

            object parameters = new { @StatType = Convert.ToInt32(model.ReportType), @EndDate = Convert.ToDateTime(model.EndDate) };
            return _genericRepository.SpExecuteSql<TopTwentyWinnersandLosers>(Constant.SPTopTwentyWinnersandLosers, parameters);
        }
        public IEnumerable<PatronCardedRevenueTrends> GetPatronCardedRevenueTrends(ReportSearchEntity model)
        {

            object parameters = new { @ReportType = Convert.ToInt32(model.ReportType), @EndDate = Convert.ToDateTime(model.EndDate) };
            return _genericRepository.SpExecuteSql<PatronCardedRevenueTrends>(Constant.SPPatronCardedRevenueTrends, parameters);
        }
        public IEnumerable<HostedPlayerActivity> GetHostedPlayerActivity(ReportSearchEntity model)
        {

            object parameters = new { @StartDate = Convert.ToDateTime(model.StartDate), @EndDate = Convert.ToDateTime(model.EndDate) };
            return _genericRepository.SpExecuteSql<HostedPlayerActivity>(Constant.SPHostedPlayerActivity, parameters);
        }
        public IEnumerable<PatronListingMain> GetPatronListing(ReportSearchEntity model)
        {

            object parameters = new { @FromDate = Convert.ToDateTime(model.StartDate), @ToDate = Convert.ToDateTime(model.EndDate), @Host = Convert.ToString(model.HostId), @PointsEarned = Convert.ToInt32(model.Points), @Visits = Convert.ToInt32(model.Visits), @TheoWin = Convert.ToInt32(model.Theo), @ADT = Convert.ToInt32(model.ADT), @BirthMonth = Convert.ToInt32(model.Birth), @AnniversaryMonth = Convert.ToInt32(model.Anniversary) };



            return _genericRepository.SpExecuteSql<PatronListingMain>(Constant.SPPatronListingMain, parameters);
        }
        public IEnumerable<NewMemberbyADTorADAReport> GetNewMemberbyADTorADAReport(ReportSearchEntity model)
        {

            object parameters = new { @StartDate = Convert.ToDateTime(model.StartDate), @EndDate = Convert.ToDateTime(model.EndDate), @Amount = Convert.ToInt32(model.Amount), @Type = Convert.ToInt32(model.Type), @ReportType = Convert.ToInt32(model.ReportType) };
            return _genericRepository.SpExecuteSql<NewMemberbyADTorADAReport>(Constant.SPNewMemberbyADTorADAReport, parameters);
        }

        public ChartInpuControlData GetChartInputData()
        {
            ChartInpuControlData dataToRtn = new ChartInpuControlData();
            
            return _genericRepository.ExecuteSPForChartInpuControlData(Constant.SPReportChartInpuControlData);
          
        }

        public ChartData GetChartData(ReportSearchEntity model)
        {
            ChartData dataToRtn = new ChartData();
            object parameters = new { @Begin = Convert.ToInt32(model.Begin), @End = Convert.ToInt32(model.End) };
            return _genericRepository.ExecuteSPForChartData(Constant.SPReportChartData, parameters);
           
        }
    }
}
