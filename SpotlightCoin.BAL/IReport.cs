﻿using SpotlightCoin.Model.Domain;
using System.Collections.Generic;

namespace SpotlightCoin.BAL
{
    public interface IReport
    {
        IEnumerable<TopTwentyWinnersandLosers> GetTopTwentyWinnersandLosers(ReportSearchEntity model);
        IEnumerable<TableswithPlaybyDay> GetTableswithPlaybyDay(ReportSearchEntity model);
        IEnumerable<PatronListingMain> GetPatronListing(ReportSearchEntity model);
        IEnumerable<PatronCardedRevenueTrends> GetPatronCardedRevenueTrends(ReportSearchEntity model);
        IEnumerable<NewMemberbyADTorADAReport> GetNewMemberbyADTorADAReport(ReportSearchEntity model);
        IEnumerable<NewAcquisitionbyHostbyMonth> GetNewAcquisitionbyHostbyMonth(ReportSearchEntity model);
        IEnumerable<HostedPlayerActivity> GetHostedPlayerActivity(ReportSearchEntity model);
        IEnumerable<DailyRevenueReport> GetDailyRevenueReport(ReportSearchEntity model);
        IEnumerable<DailyPOSRevenueFiscalYOY> GetDailyPOSRevenueFiscalYOY(ReportSearchEntity model);
        IEnumerable<HostedPlayerActivityHost> GetHostList(ReportSearchEntity model);
        ChartInpuControlData GetChartInputData();
        ChartData GetChartData(ReportSearchEntity model);
    }
}
