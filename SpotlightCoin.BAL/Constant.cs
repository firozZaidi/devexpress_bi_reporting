﻿namespace SpotlightCoin.BAL
{
    public static class Constant
    {
        public static readonly string SPDailyRevenueReport = "DailyRevenueReport";
        public static readonly string SPHostedPlayerActivity = "reporting.HostedPlayerActivity_BI";
        public static readonly string SPMonthEndSlotMarketingAnalysisFiscalNewSignups = "MonthEndSlotMarketingAnalysisFiscalNewSignups";
        public static readonly string SPNewAcquisitionbyHostbyMonth = "NewAcquisitionbyHostbyMonth";
        public static readonly string SPNewMemberbyADTorADAReport = "NewMemberbyADTorADAReport";
        public static readonly string SPPatronCardedRevenueTrends = "PatronCardedRevenueTrends";
        public static readonly string SPPatronListingMain = "PatronListingMain";
        public static readonly string SPTableswithPlaybyDay = "TableswithPlaybyDay";
        public static readonly string SPTopTwentyWinnersandLosers = "TopTwentyWinnersandLosers";
        public static readonly string SPDailyPOSRevenueFiscalYOY = "DailyPOSRevenueFiscalYOY";
        public static readonly string SPHostList = "reporting.Param_HOSTID_BI";        
        public static readonly string SPReportChartInpuControlData = "ReportChartInpuControlData";
        public static readonly string SPReportChartData = "ReportChartData";

    }
}
