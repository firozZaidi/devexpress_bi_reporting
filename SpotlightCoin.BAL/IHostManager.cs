﻿using SpotlightCoin.Model.Domain;
using System.Collections.Generic;

namespace SpotlightCoin.BAL
{
    public interface IHostManager
    {
        IEnumerable<Host> GetAllHosts();

        bool IsValidLogin(Host host);
    }
}
