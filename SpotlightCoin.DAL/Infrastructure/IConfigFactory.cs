﻿using System.Data;

namespace SpotlightCoin.DAL.Infrastructure
{
    public interface IConfigFactory
    {
        IDbConnection GetConnection { get; }
        int GetTimeOut { get; }

        string GetConextLDAP { get; }

        IDbConnection GetSpotlightConnection { get; }
    }
}
