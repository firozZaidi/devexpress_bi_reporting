﻿using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;

namespace SpotlightCoin.DAL.Infrastructure
{
    public class ConfigFactory : IConfigFactory
    {
        private readonly string dataMartConnectionString;
        private readonly string spotlightConnectionString;
        private readonly string LDAPConnection;
        private readonly string ConextLDAP;
        private readonly bool LDAPEnabled;
        private readonly int connectionTimeOut;


        public ConfigFactory(IConfiguration configuration)
        {
            this.dataMartConnectionString = configuration.GetConnectionString("DefaultConnection");
            this.spotlightConnectionString = configuration.GetConnectionString("SpotlightConnection");
            this.LDAPEnabled = Convert.ToBoolean((configuration.GetSection("LDAP:LDAPEnabled").Value), null);
            this.LDAPConnection = configuration.GetSection("LDAP:LDAPConnection").Value;
            this.ConextLDAP = configuration.GetSection("LDAP:ContextLDAP").Value;

            int TimeOut;

            this.connectionTimeOut = int.TryParse(configuration.GetSection("ConnectionStrings:ConnectionTimeOut").Value, out TimeOut)
            ? TimeOut : 20000;

        }


        public IDbConnection GetConnection
        {
            get
            {
                return new SqlConnection(dataMartConnectionString);
            }
        }

        public IDbConnection GetSpotlightConnection
        {
            get
            {
                return new SqlConnection(spotlightConnectionString);
            }
        }

        public int GetTimeOut
        {
            get
            {
                return this.connectionTimeOut;
            }

        }

        public string GetConextLDAP
        {
            get
            {
                return this.ConextLDAP;
            }

        }
    }
}
