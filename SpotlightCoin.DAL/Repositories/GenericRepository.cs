﻿using Dapper;
using SpotlightCoin.DAL.Infrastructure;
using SpotlightCoin.Model.Domain;
using System.Collections.Generic;
using System.Data;
using static Dapper.SqlMapper;

namespace SpotlightCoin.DAL.Repositories
{
    public class GenericRepository : IGenericRepository
    {

        public IDbConnection Connection { get; set; }
        int timeOut = 0;
        public GenericRepository(IConfigFactory configFactory)
        {
            Connection = configFactory.GetConnection;
            timeOut = configFactory.GetTimeOut;
        }


        public IEnumerable<T> ExecuteSP<T>(string sQuery)
        {
            IEnumerable<T> report;
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                report = dbConnection.Query<T>(sQuery, commandTimeout: timeOut, commandType: CommandType.StoredProcedure);
                return report;

            }
        }


        public ChartInpuControlData ExecuteSPForChartInpuControlData(string sQuery)
        {
            GridReader report;
            ChartInpuControlData dataToRtn = new ChartInpuControlData();
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                report = dbConnection.QueryMultiple(sQuery, commandTimeout: timeOut, commandType: CommandType.StoredProcedure);
                dataToRtn.AddData(report.Read<ReportYearStart>(), report.Read<ReportYearEnd>());
            }

            return dataToRtn;
        }

        public ChartData ExecuteSPForChartData(string sQuery, object parameters)
        {
            GridReader report;
            ChartData dataToRtn = new ChartData();
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                report = dbConnection.QueryMultiple(sQuery, parameters, commandTimeout: timeOut, commandType: CommandType.StoredProcedure);
                dataToRtn.AddData(report.Read<ReportMain>(), report.Read<ReportFiscalNewSignups>(), report.Read<ReportTotalCoinIn>());

            }
            return dataToRtn;
        }

        public IEnumerable<T> SpExecuteSql<T>(string sql, object parameters)
        {
            string message = string.Empty;
            try
            {
                IEnumerable<T> report;
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();

                    report = dbConnection.Query<T>(sql, parameters, commandTimeout: timeOut, commandType: CommandType.StoredProcedure);

                    return report;
                }
            }
            catch
            {
                throw;
            }
        }


        public IEnumerable<T> SelectAll<T>()
        {
            return null;
        }


        public void Delete<T>(object id)
        {

        }

        public void Save()
        {

        }


    }
}
