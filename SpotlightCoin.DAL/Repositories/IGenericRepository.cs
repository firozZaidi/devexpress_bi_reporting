﻿using SpotlightCoin.Model.Domain;
using System.Collections.Generic;

namespace SpotlightCoin.DAL.Repositories
{
    public interface IGenericRepository
    {
        IEnumerable<T> ExecuteSP<T>(string sQuery);
        ChartInpuControlData ExecuteSPForChartInpuControlData(string sQuery);
        ChartData ExecuteSPForChartData(string sQuery, object parameters);
        IEnumerable<T> SpExecuteSql<T>(string sQuery, object parameters);
        IEnumerable<T> SelectAll<T>();
        void Delete<T>(object id);
        void Save();
    }
}
