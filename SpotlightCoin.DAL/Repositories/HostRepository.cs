﻿using SpotlightCoin.DAL.Infrastructure;
using SpotlightCoin.Model.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices.AccountManagement;
using static Dapper.SqlMapper;

namespace SpotlightCoin.DAL.Repositories
{
    public class HostRepository : IHostRepository
    {
        private readonly IDbConnection Connection;
        private readonly string ContextLDAP;
        int timeOut = 0;
        public HostRepository(IConfigFactory configFactory)
        {
            this.Connection = configFactory.GetSpotlightConnection;
            this.timeOut = configFactory.GetTimeOut;
            this.ContextLDAP = configFactory.GetConextLDAP;
        }

        public IEnumerable<Host> GetAllHost()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open(); //x.DeletedDateTime == null && x.ActiveFlag == "Y"
                IEnumerable<Host> hosts = dbConnection.Query<Host>("SELECT ActiveFlag,HostEmail,HostFirstName,HostLastName,HostPassword,LDAPUserName,HostUserId,HostUserName,HostsId FROM Hosts WHERE ActiveFlag ='Y' AND DeletedDateTime is null AND AllowLogin=1 ORDER BY HostFirstName", commandType: CommandType.Text);
                return hosts;
            }
        }

        public bool IsValidLogin(Host host)
        {

            var context = (ContextType)Enum.Parse(typeof(ContextType), this.ContextLDAP);
            var adContext = new PrincipalContext(context);
            using (adContext)
            {
                var isValid = adContext.ValidateCredentials(host.LDAPUserName, host.HostPassword);

                return isValid;
            }
        }
    }
}
