﻿using SpotlightCoin.Model.Domain;
using System.Collections.Generic;


namespace SpotlightCoin.DAL.Repositories
{
    public interface IHostRepository
    {
        IEnumerable<Host> GetAllHost();

        bool IsValidLogin(Host host);
    }
}
